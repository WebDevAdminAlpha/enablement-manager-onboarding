## [First Name], Welcome to GitLab and the Enablement Section

We are all excited that you are joining us as a manager in the [Enablement section](https://about.gitlab.com/handbook/engineering/development/enablement). You should have already received an onboarding issue from the [People Group](https://about.gitlab.com/handbook/people-group/) as well as a new manager onboarding issue.  This onboarding issue is specific to Engineering Managers in the Enablement Section.

This tasks in this issue are grouped by responsible individual(s).  Just focus on "New Team Member" items, and feel free to reach out to your manager or peer managers if you have any questions.

### New Team Member
* [ ] Register an [access request](https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/) to establish a sub-group on GitLab (if it does not exist). [Example](https://gitlab.com/gitlab-com/access-requests/issues/2087)
* [ ] After sub-group is established, create projects for team-specific issues, onboarding, etc. See other team sub-groups for examples:
  * [Memory Team](https://gitlab.com/gitlab-org/memory-team)
  * [Distribution Team](https://gitlab.com/gitlab-org/distribution)
* [ ] Join the following Slack channels:
  * [ ] [#backend-hiring](https://gitlab.slack.com/messages/C393D69L0)
  * [ ] [#eng-week-in-review](https://gitlab.slack.com/messages/CJWA4E9UG)
  * [ ] [#s_enablement](https://gitlab.slack.com/messages/CMPK8QBB9)
  * [ ] [#dev-enablement](https://gitlab.slack.com/messages/CHJK0EVAM)
  * [ ] [#ceo](https://gitlab.slack.com/messages/C3MAZRM8W) (suggested)
  * [ ] [#vpe](https://gitlab.slack.com/messages/C9X79MNJ3) (suggested)
* [ ] Create your team channel if it does not yet exist. #g_<team name>, example: #g_geo
* [ ] Take ownership of team handbook page under [Enablement section](https://about.gitlab.com/handbook/engineering/development/enablement/)
  * Example: https://about.gitlab.com/handbook/engineering/development/enablement/memory/
* [ ] Review or create team onboarding template. This should be a template specific to your team, not the general GitLab onboarding template. Examples:
  * [Memory Team Template](https://gitlab.com/gitlab-org/memory-team/memory-team-onboarding/blob/master/.gitlab/issue_templates/Memory-team_onboarding.md)
  * [Distribution Team Template](https://gitlab.com/gitlab-org/distribution/team-tasks/blob/master/.gitlab/issue_templates/Team-onboarding.md)
  * [ ] Sub-group [@gitlab-com/backend-managers](https://gitlab.com/gitlab-com/backend-managers) or [@gitlab-com/frontend-managers](https://gitlab.com/gitlab-org/frontend/frontend-managers) via [Access Request](https://gitlab.com/gitlab-com/access-requests) using Single User Access Request

### Peer Managers
* [ ] Invite to Enablement managers coffee chat
* [ ] Invite to skip level meeting with Senior Director of Development

### Manager
Add new hire to:
* [ ] Basic BE Entitlement Access Request
* [ ] Google Group enablement-section@gitlab.com
* [ ] Manager meeting
* [ ] Slack channels:
  * [ ] [#fe-hiring-management](https://gitlab.slack.com/messages/GE2MG5M4H)
  * [ ] [#enablement-hiring](https://gitlab.slack.com/messages/GJMN3EK4Y)
  * [ ] [#enablement-leaders](https://gitlab.slack.com/messages/GMLB4U8EN)
* [ ] Member of [@gitlab-com/people-group/hiring-processes](https://gitlab.com/gitlab-com/people-group/hiring-processes/tree/master/Engineering)
* [ ] Request to be added to group triage reports if applicable. [Example](https://gitlab.com/gitlab-org/gitlab/issues/33531)
* [ ] Add to [https://gitlab.com/groups/gl-technical-interviews/backend/-/group_members](https://gitlab.com/groups/gl-technical-interviews/backend/-/group_members) as Owner
* [ ] Create an engineering [new manager enablement issue](https://gitlab.com/gitlab-com/people-group/Training/blob/master/.gitlab/issue_templates/new-manager-enablement.md)

/confidential
/due in 14 days